import * as React from "react"
import { MDXRenderer } from "gatsby-plugin-mdx"
import { graphql } from 'gatsby'
import { Helmet } from "react-helmet"
import { StaticImage } from "gatsby-plugin-image"
import '../styles.sass'

const IndexPage = ({ data }) => (
  <main>
    <Helmet>
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora" />
      <title>North Phoenix Food Not Bombs</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta name="description" content="North Phoenix Food Not Bombs is an autonomous chapter of the Food Not Bombs network operating in the northern villages of Phoenix, Arizona." />
      <meta property="og:title" content="North Phoenix Food Not Bombs" />
      <meta property="og:description" content="North Phoenix Food Not Bombs is an autonomous chapter of the Food Not Bombs network operating in the northern villages of Phoenix, Arizona." />
      <meta property="og:image" content={data.file.publicURL} />
      <meta property="og:type" content="website" />
      <meta name="twitter:card" content="summary_large_image"/>
      <meta name="twitter:site" content="@nphxfnb" />
      <meta name="twitter:creator" content="@nphxfnb"/>
      <meta name="twitter:title" content="North Phoenix Food Not Bombs" />
      <meta name="twitter:description" content="North Phoenix Food Not Bombs is an autonomous chapter of the Food Not Bombs network operating in the northern villages of Phoenix, Arizona." />
      <meta name="twitter:image" content={data.file.publicURL} />
    </Helmet>
    <section className="hero is-small has-background-black-bis is-dark">
      <div className="hero-body">
        <div className="container flex align-center justify-center">
          <StaticImage
            src="../images/logo-with-text.png"
            alt="Food Not Bombs logo"
          />
        </div>
      </div>
    </section>
    {data.allMdx.nodes.map((mdx) => (
      <section className="section" key={mdx.frontmatter.title}>
        <div className="container is-max-desktop">
          <div className="content">
            <h2 className="title is-size-3-mobile">{mdx.frontmatter.title}</h2>
            <MDXRenderer>{mdx.body}</MDXRenderer>
          </div>
        </div>
      </section>
    ))}
    <footer className="footer" />
  </main>
);

export const pageQuery = graphql`
  query MDXQuery {
    file(name: { eq: "logo-with-wrapped-text" }) {
      publicURL
    }
    allMdx(sort: {fields: frontmatter___order})  {
      nodes {
        frontmatter {
          title
        }
        body
      }
    }
  }
`

export default IndexPage
